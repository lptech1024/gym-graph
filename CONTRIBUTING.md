# Setup

## Git authentication

1. [Generate SSH key pair](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#about-ssh-key-generation)
2. [Set private key](https://man.openbsd.org/ssh_config.5#IdentityFile) (optional)
3. [Add public key to account](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)

# Commits

[About Commits](/documentation/about-commits.md)

# Formatting

## Non-code

Non-code files should default to [CommonMark](https://commonmark.org/), a widely-used markup language

### Restrictions

Use "\\" for line breaks\
Use "." for ordered lists

# Versioning

[Semantic Versioning 2.0.0](https://semver.org/)

MAJOR.MINOR.PATCH\
9.55.2

Increment:
1. MAJOR version for incompatible API changes
2. MINOR version for new backward-compatible functionality
3. PATCH version for backward-compatible bug fixes
